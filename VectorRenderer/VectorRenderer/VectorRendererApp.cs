﻿using SharpDX;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.Windows;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Buffer = SharpDX.Direct3D11.Buffer;
using Device = SharpDX.Direct3D11.Device;

namespace VectorRenderer
{
    struct RadVector
    {
        public Vector3 start;
        public Vector3 vector;
        public Color4 color;
    }

    class VectorRendererApp : DXApp
    {
        List<RadVector> radVectors = new List<RadVector>();
        Vertex[] vectorVertices = ModelCreator.CreateCylinder();
        Vertex[] sourceVertices = ModelCreator.CreateRectangle();
        Buffer constantBuffer;
        Buffer colorConstantBuffer;
        Matrix view;
        Matrix proj;
        Vector3 cameraPos;
        Vector2 cameraLook;
        Vector3 camFwd;

        Buffer vertices;
        Buffer axesVertices;
        Buffer srcVertices;

        Texture2D backBuffer;
        RenderTargetView renderView;
        Texture2D depthBuffer;
        DepthStencilView depthView;

        bool userResized;

        Vector2 mousePrevPos;
        private string v;

        public VectorRendererApp()
            : this("vectors.txt")
        {
        }

        public VectorRendererApp(string f)
        {
            var lines = File.ReadAllLines(f);
            foreach (var line in lines) {
                radVectors.Add(ParseRadVector(line));
            }
        }

        private RadVector ParseRadVector(string line)
        {
            var tokens = line.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(ln => ln.Replace('.', ',')).ToArray();
            var start = new Vector3(Single.Parse(tokens[0]), Single.Parse(tokens[1]), Single.Parse(tokens[2]));
            return new RadVector() {
                start = start / 20,
                vector = new Vector3(Single.Parse(tokens[3]), Single.Parse(tokens[4]), Single.Parse(tokens[5])) / 20,
                color = new Color4(Int32.Parse(tokens[6]) / 255.0f, Int32.Parse(tokens[7]) / 255.0f, Int32.Parse(tokens[8]) / 255.0f, 1)
            };
        }

        protected override void Init()
        {
            var vertexShaderByteCode = ShaderBytecode.CompileFromFile("shader.fx", "VS", "vs_4_0");
            var vertexShader = new VertexShader(device, vertexShaderByteCode);

            var pixelShaderByteCode = ShaderBytecode.CompileFromFile("shader.fx", "PS", "ps_4_0");
            var pixelShader = new PixelShader(device, pixelShaderByteCode);

            var signature = ShaderSignature.GetInputSignature(vertexShaderByteCode);
            // Layout from VertexShader input signature
            var layout = new InputLayout(device, signature, new[]
                    {
                        new InputElement("POSITION", 0, Format.R32G32B32A32_Float, 0, 0)
                    });

            // Creates the VertexBuffer
            vertices = Buffer.Create(device, BindFlags.VertexBuffer, vectorVertices);
            axesVertices = Buffer.Create(device, BindFlags.VertexBuffer, new[] {
                new Vector4(0, 0, 0, 1), new Vector4(1, 0, 0, 1)
            });
            srcVertices = Buffer.Create(device, BindFlags.VertexBuffer, sourceVertices);
            constantBuffer = new Buffer(device, Utilities.SizeOf<Matrix>(), ResourceUsage.Default, BindFlags.ConstantBuffer, CpuAccessFlags.None, ResourceOptionFlags.None, 0);
            colorConstantBuffer = new Buffer(device, Utilities.SizeOf<Vector4>(), ResourceUsage.Default, BindFlags.ConstantBuffer, CpuAccessFlags.None, ResourceOptionFlags.None, 0);

            
            context.InputAssembler.InputLayout = layout;
            context.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;
            context.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(vertices, Utilities.SizeOf<Vertex>(), 0));
            context.VertexShader.SetConstantBuffer(0, constantBuffer);
            context.VertexShader.SetConstantBuffer(1, colorConstantBuffer);
            context.VertexShader.Set(vertexShader);
            context.PixelShader.Set(pixelShader);

            // Prepare matrices
            cameraPos = new Vector3(10, 10, 10);
            cameraLook = new Vector2(4,-0.5f);
            proj = Matrix.PerspectiveFovRH((float)Math.PI / 4.0f, form.ClientSize.Width / (float)form.ClientSize.Height, 0.1f, 100.0f);
            form.UserResized += (sender, args) => userResized = true;
            userResized = true;

            form.MouseMove += Form_MouseMove;
        }

        private void Form_MouseMove(object sender, MouseEventArgs e)
        {
            var delta = new Vector2(e.X, e.Y) - mousePrevPos;
            mousePrevPos = new Vector2(e.X, e.Y);
            if (e.Button != MouseButtons.Left) return;
            cameraLook.X -= delta.X * 0.008f;
            cameraLook.Y -= delta.Y * 0.008f;
            float eps = 0.001f;
            if (cameraLook.Y > Math.PI / 2 - eps)
                cameraLook.Y = (float)(Math.PI / 2 - eps);
            if (cameraLook.Y < -Math.PI / 2 + eps)
                cameraLook.Y = (float)(-Math.PI / 2 + eps);
        }

        protected override void Update(int msStep)
        {
            camFwd = new Vector3(1, 0, 0);
            var x = Matrix.RotationZ(cameraLook.X);
            camFwd = Vector3.TransformNormal(camFwd, x);
            var side = Vector3.Cross(camFwd, Vector3.UnitZ);
            var y = Matrix.RotationAxis(side, cameraLook.Y);
            camFwd = Vector3.TransformNormal(camFwd, y);

            var move = camFwd * 0.1f;
            var sideMove = side * 0.1f;
            if (IsKeyPressed(Keys.W)) {
                cameraPos += move;
            }
            if (IsKeyPressed(Keys.S)) {
                cameraPos -= move;
            }
            if (IsKeyPressed(Keys.A)) {
                cameraPos -= sideMove;
            }
            if (IsKeyPressed(Keys.D)) {
                cameraPos += sideMove;
            }
            if (IsKeyPressed(Keys.R)) {
                cameraPos.Z += 0.1f;
            }
            if (IsKeyPressed(Keys.F)) {
                cameraPos.Z -= 0.1f;
            }
        }

        protected override void Draw()
        {
            CheckResized();
            // Clear views
            context.ClearDepthStencilView(depthView, DepthStencilClearFlags.Depth, 1.0f, 0);
            context.ClearRenderTargetView(renderView, Color.Black);
            
            var blendState = new BlendStateDescription();
            var blendDesc = new RenderTargetBlendDescription(
                true,
                BlendOption.SourceAlpha,
                BlendOption.InverseSourceAlpha,
                BlendOperation.Add,
                BlendOption.One,
                BlendOption.Zero,
                BlendOperation.Add,
                ColorWriteMaskFlags.All);
            blendState.RenderTarget[0] = blendDesc;
            var bs = new BlendState(device, blendState);

            context.OutputMerger.SetBlendState(bs);
            view = Matrix.LookAtRH(cameraPos, cameraPos + camFwd, Vector3.UnitZ);
            DrawAxes();
            DrawSource(new Color4(1, 1, 0, 1));
            for (int i = 0; i < radVectors.Count; i++) {
                var radVec = radVectors[i];
                DrawVector(radVec.start, radVec.vector, radVec.color);
            }
            DrawChunk();
            //DrawVector(new Vector3(1, 0.5f, 0), new Vector3(0.0f, -1.0f, 3f), new Color4(0, 1, 0, 1.0f));
            //DrawVector(new Vector3(1.5f, 0, 1), new Vector3(-0.3f, 0.3f, 1), new Color4(0, 0, 1, 1.0f));

            // Present!
            swapChain.Present(1, PresentFlags.None);
        }

        private void DrawChunk()
        {
            context.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;
            context.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(vertices, Utilities.SizeOf<Vertex>(), 0));
            var viewProj = Matrix.Multiply(view, proj);
            var worldViewProj = Matrix.Scaling(2.5f, 2.5f, 4) * viewProj;
            worldViewProj.Transpose();
            var color = new Color4(1.0f, 160 / 256.0f, 0, 0.35f);
            context.UpdateSubresource(ref worldViewProj, constantBuffer);
            context.UpdateSubresource(ref color, colorConstantBuffer);
            context.Draw(vectorVertices.Length, 0);
        }

        private void DrawAxes()
        {
            context.InputAssembler.PrimitiveTopology = PrimitiveTopology.LineList;
            context.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(axesVertices, Utilities.SizeOf<Vertex>(), 0));
            var viewProj = Matrix.Multiply(view, proj);

            var worldViewProj = Matrix.Scaling(100f, 100f, 1)  * viewProj; worldViewProj.Transpose();
            Color4 color = new Color4(1, 1, 1, 1);
            context.UpdateSubresource(ref worldViewProj, constantBuffer); context.UpdateSubresource(ref color, colorConstantBuffer);
            context.Draw(2, 0);

            worldViewProj = Matrix.Scaling(-100f, -100f, 1) * viewProj; worldViewProj.Transpose();
            color = new Color4(0.5f, 0.5f, 0.5f, 1);
            context.UpdateSubresource(ref worldViewProj, constantBuffer); context.UpdateSubresource(ref color, colorConstantBuffer);
            context.Draw(2, 0);

            worldViewProj = Matrix.RotationZ((float)Math.PI / 2) * Matrix.Scaling(1, 100f, 1) * viewProj; worldViewProj.Transpose();
            color = new Color4(1, 1, 1, 1);
            context.UpdateSubresource(ref worldViewProj, constantBuffer); context.UpdateSubresource(ref color, colorConstantBuffer);
            context.Draw(2, 0);

            worldViewProj = Matrix.RotationZ((float)Math.PI / 2) * Matrix.Scaling(1, -100f, 1) * viewProj; worldViewProj.Transpose();
            color = new Color4(0.5f, 0.5f, 0.5f, 1);
            context.UpdateSubresource(ref worldViewProj, constantBuffer); context.UpdateSubresource(ref color, colorConstantBuffer);
            context.Draw(2, 0);

            worldViewProj = Matrix.RotationY(-(float)Math.PI / 2) * Matrix.Scaling(1, 1, 100) * viewProj; worldViewProj.Transpose();
            color = new Color4(1, 1, 1, 1);
            context.UpdateSubresource(ref worldViewProj, constantBuffer); context.UpdateSubresource(ref color, colorConstantBuffer);
            context.Draw(2, 0);

            worldViewProj = Matrix.RotationY(-(float)Math.PI / 2) * Matrix.Scaling(1, 1, -100) * viewProj; worldViewProj.Transpose();
            color = new Color4(0.5f, 0.5f, 0.5f, 1);
            context.UpdateSubresource(ref worldViewProj, constantBuffer); context.UpdateSubresource(ref color, colorConstantBuffer);
            context.Draw(2, 0);
        }

        private void DrawSource(Color4 color)
        {
            var descRaster = RasterizerStateDescription.Default();
            descRaster.CullMode = CullMode.None;
            context.Rasterizer.State = new RasterizerState(device, descRaster);
            context.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;
            context.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(srcVertices, Utilities.SizeOf<Vertex>(), 0));
            var viewProj = Matrix.Multiply(view, proj);
            var worldViewProj = Matrix.Scaling(1, 1, 0.01f) * viewProj;
            worldViewProj.Transpose();
            context.UpdateSubresource(ref worldViewProj, constantBuffer);
            context.UpdateSubresource(ref color, colorConstantBuffer);
            context.Draw(sourceVertices.Length, 0);
            descRaster = RasterizerStateDescription.Default();
            context.Rasterizer.State = new RasterizerState(device, descRaster);
        }

        private void DrawVector(Vector3 startPoint, Vector3 vector, Color4 color)
        {
            context.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;
            context.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(vertices, Utilities.SizeOf<Vertex>(), 0));
            var viewProj = Matrix.Multiply(view, proj);
            var len = vector.Length();
            vector.Normalize();
            
            var y1 = Matrix.RotationY((float)((Math.PI / 2) - Math.Asin(vector.Z)));
            var z1 = Matrix.RotationZ((float)(Math.Atan2(vector.Y, vector.X)));
           /* var z = Matrix.RotationZ((float)(Math.Atan2(vector.Y, vector.X)));
            var side = Vector3.Cross(vector, Vector3.UnitZ);
            side.Normalize();
            var s = Matrix.RotationAxis(-side, (float)((Math.PI / 2) - Math.Asin(vector.Z)));*/
            var worldViewProj = Matrix.Scaling(0.01f, 0.01f, len)  *y1*z1 * Matrix.Translation(startPoint) * viewProj; 
            worldViewProj.Transpose();
            Vector3 sap = new Vector3(0, 0, 1);
            sap = Vector3.TransformNormal(sap, y1*z1);
            context.UpdateSubresource(ref worldViewProj, constantBuffer);
            context.UpdateSubresource(ref color, colorConstantBuffer);
            context.Draw(vectorVertices.Length, 0);
        }

        private void CheckResized()
        {
            if (userResized) {
                // Dispose all previous allocated resources
                Utilities.Dispose(ref backBuffer);
                Utilities.Dispose(ref renderView);
                Utilities.Dispose(ref depthBuffer);
                Utilities.Dispose(ref depthView);

                // Resize the backbuffer
                swapChain.ResizeBuffers(desc.BufferCount, form.ClientSize.Width, form.ClientSize.Height, Format.Unknown, SwapChainFlags.None);

                // Get the backbuffer from the swapchain
                backBuffer = Texture2D.FromSwapChain<Texture2D>(swapChain, 0);

                // Renderview on the backbuffer
                renderView = new RenderTargetView(device, backBuffer);

                // Create the depth buffer
                depthBuffer = new Texture2D(device, new Texture2DDescription() {
                    Format = Format.D32_Float_S8X24_UInt,
                    ArraySize = 1,
                    MipLevels = 1,
                    Width = form.ClientSize.Width,
                    Height = form.ClientSize.Height,
                    SampleDescription = new SampleDescription(1, 0),
                    Usage = ResourceUsage.Default,
                    BindFlags = BindFlags.DepthStencil,
                    CpuAccessFlags = CpuAccessFlags.None,
                    OptionFlags = ResourceOptionFlags.None
                });

                // Create the depth buffer view
                depthView = new DepthStencilView(device, depthBuffer);

                // Setup targets and viewport for rendering
                context.Rasterizer.SetViewport(new Viewport(0, 0, form.ClientSize.Width, form.ClientSize.Height, 0.0f, 1.0f));
                context.OutputMerger.SetTargets(depthView, renderView);

                // Setup new projection matrix with correct aspect ratio
                proj = Matrix.PerspectiveFovRH((float)Math.PI / 4.0f, form.ClientSize.Width / (float)form.ClientSize.Height, 0.1f, 100.0f);
                //proj = Matrix.OrthoRH(form.ClientSize.Width /100, form.ClientSize.Height/100, -10000.0f, 10000.0f);
                userResized = false;
            }
        }
    }
}
