﻿using SharpDX;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.Windows;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorRenderer
{
    abstract class DXApp
    {
        protected DeviceContext context;
        protected SharpDX.Direct3D11.Device device;
        protected RenderForm form;

        protected SwapChainDescription desc;
        protected SwapChain swapChain;

        protected abstract void Init();
        protected abstract void Update(int msStep);
        protected abstract void Draw();

        Dictionary<Keys, bool> pressedKeys = new Dictionary<Keys, bool>();

        internal void Initialize()
        {
            form = new RenderForm("VectorRenderer");
            //form.AllowUserResizing = false;
            desc = new SwapChainDescription() {
                BufferCount = 1,
                ModeDescription =
                   new ModeDescription(form.ClientSize.Width, form.ClientSize.Height,
                                       new Rational(60, 1), Format.R8G8B8A8_UNorm),
                IsWindowed = true,
                OutputHandle = form.Handle,
                SampleDescription = new SampleDescription(1, 0),
                SwapEffect = SwapEffect.Discard,
                Usage = Usage.RenderTargetOutput
            };

            // Creates the Device
            SharpDX.Direct3D11.Device.CreateWithSwapChain(DriverType.Hardware, DeviceCreationFlags.None, desc, out device, out swapChain);
            context = device.ImmediateContext;

            var factory = swapChain.GetParent<Factory>();
            factory.MakeWindowAssociation(form.Handle, WindowAssociationFlags.IgnoreAll);

            form.KeyDown += Form_KeyDown;
            form.KeyUp += Form_KeyUp;
        }

        private void Form_KeyUp(object sender, KeyEventArgs e)
        {
            pressedKeys[e.KeyCode] = false;
        }

        private void Form_KeyDown(object sender, KeyEventArgs e)
        {
            pressedKeys[e.KeyCode] = true;
        }

        protected bool IsKeyPressed(Keys key)
        {
            bool result;
            if (!pressedKeys.TryGetValue(key, out result)) {
                pressedKeys[key] = false;
            }
            return result;
        }

        internal void Run()
        {
            Init();
            var clock = new Stopwatch();
            clock.Start();

            RenderLoop.Run(form, () => {
                Update((int)clock.ElapsedMilliseconds);
                clock.Restart();
                Draw();
            });
        }
    }
}
