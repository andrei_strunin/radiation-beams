﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace VectorRenderer
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct Vertex
    {
        public Vector4 Position;
    }

    static class ModelCreator
    {   
        internal static Vertex[] CreateCylinder(float fullness = 1.0f)
        {
            int n = 32;
            int z = 1;
            List<Vertex> verticies = new List<Vertex>();
            CylinderBase(n, verticies, 0, fullness);
            CylinderBase(n, verticies, z, fullness);
            float phi = 0, phiNext = 0;
            for (int i = 0; i < n; i++) {
                verticies.Add(new Vertex() {
                    Position = new Vector4((float)Math.Cos(phi), (float)Math.Sin(phi), 0, 1)
                });
                verticies.Add(new Vertex() {
                    Position = new Vector4((float)Math.Cos(phi), (float)Math.Sin(phi), z, 1)
                });
                phiNext = (float)((fullness * 2 * Math.PI) / n) * (i + 1);
                verticies.Add(new Vertex() {
                    Position = new Vector4((float)Math.Cos(phiNext), (float)Math.Sin(phiNext), z, 1)
                });

                verticies.Add(new Vertex() {
                    Position = new Vector4((float)Math.Cos(phiNext), (float)Math.Sin(phiNext), z, 1)
                });
                verticies.Add(new Vertex() {
                    Position = new Vector4((float)Math.Cos(phiNext), (float)Math.Sin(phiNext), 0, 1)
                });
               
                verticies.Add(new Vertex() {
                    Position = new Vector4((float)Math.Cos(phi), (float)Math.Sin(phi), 0, 1)
                });
                phi = phiNext;
            }

            return verticies.ToArray();
        }
        internal static Vertex[] CreateRectangle()
        {
            List<Vertex> verticies = new List<Vertex>();
            verticies.Add(new Vertex()
            {
                Position = new Vector4(1, 1, 0, 1)
            });
            verticies.Add(new Vertex()
            {
                Position = new Vector4(1, -1, 0, 1)
            });
            verticies.Add(new Vertex()
            {
                Position = new Vector4(-1, -1, 0, 1)
            });
            verticies.Add(new Vertex()
            {
                Position = new Vector4(1, 1, 0, 1)
            });
            verticies.Add(new Vertex()
            {
                Position = new Vector4(-1, 1, 0, 1)
            });
            verticies.Add(new Vertex()
            {
                Position = new Vector4(-1, -1, 0, 1)
            });
            return verticies.ToArray();
        }

        private static void CylinderBase(int n, List<Vertex> verticies, int z, float fullness)
        {
            float phi = 0;
            for (int i = 0; i < n; i++) {
                if (z != 0) {
                    verticies.Add(new Vertex() {
                        Position = new Vector4((float)Math.Cos(phi), (float)Math.Sin(phi), z, 1)
                    });
                    verticies.Add(new Vertex() {
                        Position = new Vector4(0, 0, z, 1)
                    });
                } else {
                    verticies.Add(new Vertex() {
                        Position = new Vector4(0, 0, z, 1)
                    });
                    verticies.Add(new Vertex() {
                        Position = new Vector4((float)Math.Cos(phi), (float)Math.Sin(phi), z, 1)
                    });                    
                }
                phi = (float)(((fullness * 2 * Math.PI) / n) * (i + 1));
                verticies.Add(new Vertex() {
                    Position = new Vector4((float)Math.Cos(phi), (float)Math.Sin(phi), z, 1)
                });
            }
        }
    }
}
