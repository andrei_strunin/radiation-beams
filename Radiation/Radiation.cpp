#include "stdafx.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <time.h>
#include <fstream>
#include <vector>
using namespace std;
const int dre = 21,drz=101;
//PHI - ���������� ���� � ��������� X0Y, PSI - ������������, THETA - ����������� 
//��������� ������� � ��������� ������ � ��� ����������� ������������ 
struct vectorxyz {
	double x0;
	double y0;
	double z0;
	double x;
	double y;
	double z;
};
struct point {
	double x;
	double y;
	double z;
};
//��������� �������� ����� (0,1)
double gamma() {
	return (double)rand() / RAND_MAX;
}
//����������� ������������� ��������� �������� �� � �� �
double unidib(double A, double B) {
	return (B - A)*gamma() + A;
}
//������� ��� ��������� ������� �� ��������� �� ������ ������ �� �����
double po(double r0) {
	return r0*sqrt(gamma());
}
//������� ��� ��������� ����� ���������� ������� �� ������� �������
double freerun(double CURRENT_SIGMA) {
	return -(log(gamma()) / CURRENT_SIGMA);
}
//������� �������� ������������ �������� v2 �� ����� v1
double linterpol(vector<double> v1, vector<double> v2, double x) {
	double y;
	int leftborder;
	for (int i = 1; x > v1[i]; i++)
	{
		leftborder = i;
	}
	y = v2[leftborder] + (v2[leftborder + 1] - v2[leftborder])*(x - v1[leftborder]) / (v1[leftborder + 1] - v1[leftborder]);
	return y;
}
//��������� ������
vectorxyz startingvector(double p, double phi, double w1, double w2, double w3, double l) {
	vectorxyz v;
	double length;
	v.x0 = p*cos(phi);
	v.y0 = p*sin(phi);
	v.z0 = 0;
	length = sqrt(w1*w1 + w2*w2 + w3*w3);
	v.x = w1*l / length;
	v.y = w2*l / length;
	v.z = w3*l / length;
	return v;
}
//�������� ������ �� ���������
bool ifalive(double CURRENT_REFLECT, double CURRENT_SIGMA) {
	if (gamma() > CURRENT_REFLECT / CURRENT_SIGMA) return false;
	else return true;
}
//������ ����� �������
double newalpha(double CURRENT_ALPHA) {
	double p, x;
	do
	{
		x = CURRENT_ALPHA*(1 + 2 * CURRENT_ALPHA*gamma()) / (1 + 2 * CURRENT_ALPHA);
		p = x / CURRENT_ALPHA + CURRENT_ALPHA / x + (1 / CURRENT_ALPHA - 1 / x)*(2 + 1 / CURRENT_ALPHA - 1 / x);
	} while (gamma()*(1 + 2 * CURRENT_ALPHA + 1 / (1 + 2 * CURRENT_ALPHA)) > p);
	return x;
}
//������ ����� ��������� �������
vectorxyz newvector(vectorxyz v, long double mu, double psi, double CURRENT_SIGMA) {
	vectorxyz v1;
	double length, run = freerun(CURRENT_SIGMA);
	v1.x0 = v.x + v.x0;
	v1.y0 = v.y + v.y0;
	v1.z0 = v.z + v.z0;
	length = sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
	v.x = v.x / length;
	v.y = v.y / length;
	v.z = v.z / length;
	v1.z = (v.z*mu + sqrt((1 - mu*mu)*(1 - v.z*v.z))*cos(psi));
	v1.y = (v.y*(mu - v.z*v1.z) - v.x*sin(psi)*sqrt((1 - mu*mu)*(1 - v.z*v.z))) / (1 - v.z*v.z);
	v1.x = (v.x*(mu - v.z*v1.z) - v.y*sin(psi)*sqrt((1 - mu*mu)*(1 - v.z*v.z))) / (1 - v.z*v.z);
	length = sqrt(v1.x*v1.x + v1.y*v1.y + v1.z*v1.z);
	v1.x = v1.x*run / length;
	v1.y = v1.y*run / length;
	v1.z = v1.z*run / length;
	return v1;
}
//�������� �� �������������� ������
bool ifinside(vectorxyz v, double r, double h) {
	if ((sqrt(pow(v.x0 + v.x, 2) + pow(v.y0 + v.y, 2)) <= r) && (v.z0 + v.z <= h) && (v.z0 + v.z >= 0)) return true;
	else return false;
}
string colorifinside(vectorxyz v, double r, double h) {
	if ((sqrt(pow(v.x0 + v.x, 2) + pow(v.y0 + v.y, 2)) <= r) && (v.z0 + v.z <= h)) return "0,255,0";
	else return "255,0,0";
}
//������ ������� ������ ���� � ����� � ����� ���������� 
void beamhistoryandflow(vector<double> energy, vector<double> reflect, vector<double> sigma, double CURRENT_ENERGY, double MIN_ENERGY, double r0, double r, double h, point det, fstream &fs1, bool ifwrite, double* flow, int &trait) {
	if (CURRENT_ENERGY > MIN_ENERGY)
	{
		int j = 0;
		double CURRENT_SIGMA = linterpol(energy, sigma, CURRENT_ENERGY), CURRENT_REFLECT = linterpol(energy, reflect, CURRENT_ENERGY),
			CURRENT_ALPHA = CURRENT_ENERGY / 0.511, p = po(r0), phi = unidib(0, M_PI / 2), w1 = unidib(-1, 1), w2 = unidib(-1, 1), w3 = unidib(0, 1), l = freerun(CURRENT_SIGMA),
			mu, OLD_ALPHA, distance, tau, CURRENT_WEIGHT = 1, fraction;
		string a[3] = { "0,255,0","255,0,0","0,0,255" };
		vectorxyz v = startingvector(p, phi, w1, w2, w3, l);
		if (ifinside(v, r, h))
		{
			if (ifwrite) fs1 << v.x0 << "," << v.y0 << "," << v.z0 << "," << v.x << "," << v.y << "," << v.z << "," << "0,255,0" << endl;
		}
		else trait++;
		OLD_ALPHA = CURRENT_ALPHA;
		CURRENT_ALPHA = newalpha(CURRENT_ALPHA);
		CURRENT_ENERGY = CURRENT_ALPHA*0.511;
		CURRENT_SIGMA = linterpol(energy, sigma, CURRENT_ENERGY);
		CURRENT_REFLECT = linterpol(energy, reflect, CURRENT_ENERGY);
		CURRENT_WEIGHT = CURRENT_WEIGHT*CURRENT_REFLECT / CURRENT_SIGMA;
		while (CURRENT_ENERGY>MIN_ENERGY&&ifalive(CURRENT_REFLECT, CURRENT_SIGMA) && ifinside(v, r, h))
		{
			mu = 1 - 1 / CURRENT_ALPHA + 1 / OLD_ALPHA;
			v = newvector(v, mu, unidib(0, 2 * M_PI), CURRENT_SIGMA);
			if (ifinside(v, r, h))
			{
				j++;
				if (ifwrite) fs1 << v.x0 << "," << v.y0 << "," << v.z0 << "," << v.x << "," << v.y << "," << v.z << "," << a[j].c_str() << endl;
				if (j == 2) j = 0;
			}
			else break;
			//��������� ����������������� ������� � ��������������
			fraction = 1 / (4 * M_PI)*pow(1 + OLD_ALPHA*(1 - mu), -2)*(1 + mu*mu + pow(OLD_ALPHA, 2)*pow(1 - mu, 2) / (1 + OLD_ALPHA*(1 - mu))) /
				((1 + OLD_ALPHA) / pow(OLD_ALPHA, 2)*(2 * (1 + OLD_ALPHA) / (1 + 2 * OLD_ALPHA) - log(1 + 2 * OLD_ALPHA) / OLD_ALPHA)
					+ log(1 + 2 * OLD_ALPHA) / (2 * OLD_ALPHA) - (1 + 3 * OLD_ALPHA) / pow(1 + 2 * OLD_ALPHA, 2));
			double E, de = (2.5 - MIN_ENERGY) / (dre - 1) / 2;
			for (int i = 0; i<drz; i++)
				for (int j = 0; j<dre; j++)
				{
					det.z = i;
					E = MIN_ENERGY + j * 2 * de;
					if (CURRENT_ENERGY > E - de&&CURRENT_ENERGY < E + de)
					{
						distance = sqrt(pow(det.x - (v.x0 + v.x), 2) + pow(det.y - (v.y0 + v.y), 2) + pow(det.z - (v.z0 + v.z), 2));
						tau = distance*CURRENT_SIGMA;
						*(flow + i*dre + j) += CURRENT_WEIGHT*exp(-tau)*fraction / pow(distance, 2);
					}
				}
			OLD_ALPHA = CURRENT_ALPHA;
			CURRENT_ALPHA = newalpha(CURRENT_ALPHA);
			CURRENT_ENERGY = CURRENT_ALPHA*0.511;
			CURRENT_SIGMA = linterpol(energy, sigma, CURRENT_ENERGY);
			CURRENT_REFLECT = linterpol(energy, reflect, CURRENT_ENERGY);
			CURRENT_WEIGHT = CURRENT_WEIGHT*CURRENT_REFLECT / CURRENT_SIGMA;
		}
	}
}
int main()
{
	srand(time(NULL));
	vector<double> energy;
	energy.insert(energy.end(), { 0.02, 0.03, 0.04, 0.05, 0.06, 0.08, 0.1, 0.15, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 1.0, 1.5, 2.0, 3.0 });
	vector<double> reflect;
	reflect.insert(reflect.end(), { 0.147, 0.142, 0.138, 0.134, 0.130, 0.123, 0.117, 0.106, 0.0968, 0.0843, 0.0756, 0.0689, 0.0637, 0.0561, 0.0503, 0.0410, 0.0349, 0.0274 });
	vector<double> photo;
	photo.insert(photo.end(), { 83.1, 28.5, 13.2, 7.21, 4.39, 1.97, 5.23, 1.8, 0.843, 0.289, 0.141, 0.0823, 0.0538, 0.0285, 0.0180, 0.00858, 0.00523, 0.00282 });
	vector<double> consumpt;
	consumpt.insert(consumpt.end(), { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.00159, 0.005, 0.0115 });
	vector<double> sigma;
	for (int i = 0; i<reflect.size(); i++)
	{
		sigma.insert(sigma.end(), reflect[i] + photo[i] + consumpt[i]);
	}
	double r0 = 30, r = 80, h = 70, CURRENT_ENERGY = 2.5, MIN_ENERGY = 0.02;
	int trait = 0;
	fstream fs1;
	vectorxyz v;
	/*fs1.open("dots.txt", fstream::in | fstream::out | fstream::trunc);
	for (int i = 0; i < 1000; i++)
	{
	v = startingvector(po(r0), unidib(0, M_PI / 2), unidib(-1, 1), unidib(-1, 1), unidib(0, 1), freerun(linterpol(energy, sigma, CURRENT_ENERGY)));
	fs1 << v.x0 << "," << v.y0 << "," << v.z0 << "," << 0 <<
	"," << 0 << "," << 0.5 << "," << "255,0,0" << endl;
	}
	fs1.close();
	fs1.open("startvectors.txt", fstream::in | fstream::out | fstream::trunc);
	for (int i = 0; i < 1000; i++)
	{
	v = startingvector(po(r0), unidib(0, M_PI / 2), unidib(-1,1),unidib(-1,1),unidib(0,1), freerun(linterpol(energy,sigma,CURRENT_ENERGY)));
	fs1 << v.x0 << "," << v.y0 << "," << v.z0 << "," << v.x <<
	"," << v.y << "," << v.z << "," << colorifinside(v, r, h).c_str() << endl;
	}*/
	fs1.close();
	//����� ��������������
	point det;
	det.z = 0;
	det.y = 0;
	det.x = 0;
	double flow[drz][dre];
	/*
	fs1.open("vectors.txt", fstream::in | fstream::out | fstream::trunc);
	for (int i = 0; i < 100; i++)
	beamhistoryandflow(energy, reflect, sigma, CURRENT_ENERGY, MIN_ENERGY, r0, r, h, det,fs1, true,flow,trait);
	fs1.close();*/
	for (int i = 0; i<drz; i++)
		for (int j = 0; j<dre; j++)
			flow[i][j] = 0;
	double rd = 1000000;
	fs1.open("flow.txt", fstream::in | fstream::out | fstream::trunc);
	for (int i = 0; i < rd; i++)
	{
		trait = 0;
		beamhistoryandflow(energy, reflect, sigma, CURRENT_ENERGY, MIN_ENERGY, r0, r, h, det, fs1, false, &flow[0][0], trait);
		cout << i << endl;
	}
	for (int i = 0; i < drz; i++)
	{
		for (int j = 0; j < dre; j++)
		{
			flow[i][j] = flow[i][j] / (rd - trait);
			fs1 << flow[i][j] << "	";
			cout << i << "	" << j << endl;
		}
		fs1 << endl;
	}
	fs1.close();
	system("pause");
	return 0;
}
